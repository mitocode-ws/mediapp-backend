package com.mitocode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class MediappBackendApplication extends WebMvcConfigurerAdapter { //extends SpringBootServletInitializer {

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MediappBackendApplication.class);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(MediappBackendApplication.class, args);
	}
	
}
