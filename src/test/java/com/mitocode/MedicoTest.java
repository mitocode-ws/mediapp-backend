package com.mitocode;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.mitocode.controller.MedicoController;
import com.mitocode.dao.IMedicoDAO;
import com.mitocode.model.Medico;
import com.mitocode.service.IMedicoService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MedicoTest {
	
	@Autowired
	private IMedicoService imedicoService;
	
	@Autowired
	private MedicoController medicoController;
	
	@MockBean
	private IMedicoDAO imedicoDao;
	
	@Test
	public void camposSetGet() {
		Medico medico = new Medico();	
		String nombre = "Manuel";
		String apellido = "Guevara";
		String cmp = "Fisioterapia";
				
		medico.setNombres(nombre);
		medico.setApellidos(apellido);
		medico.setCMP(cmp);
		
		assertEquals(medico.getNombres(), nombre);
		assertEquals(medico.getApellidos(), apellido);
		assertEquals(medico.getCMP(), cmp);
	}
	
	@Test
	public void crearMedico() throws Exception {		
		Medico medico = new Medico("Manuel","Guevara","Fisioterapia");
		when(imedicoDao.save(medico)).thenReturn(medico);
		assertEquals(medico, imedicoService.registrar(medico));		
		assertEquals(medico, medicoController.registrar(medico));		
	}
	
	@Test
	public void borrarMedico() {
		Medico medico = new Medico("Manuel","Guevara","Fisioterapia");
		imedicoService.eliminar(medico.getIdMedico());
		
		verify(imedicoDao,times(1)).delete(medico.getIdMedico());
		
		Medico medico1 = new Medico("Manuel","Guevara","Fisioterapia");
		medicoController.eliminar(medico1.getIdMedico());
	}
	
	@Test
	public void buscaMedico() {
		Medico medico = new Medico("Enrique","Ruiz","Odontología");
		Integer id = 2;
		medico.setIdMedico(id);		
		Medico medico1 = imedicoDao.findOne(id);		
		assertNull(medico1);
		
		Medico medico2 = imedicoService.listarId(id);
		assertNull(medico2);
	}
	
	@Test
	public void listarMedico() {		
		when(imedicoDao.findAll()).thenReturn(Stream.of(new Medico("Manuel","Guevara","Fisioterapia"),new Medico("Manue1","Guevar1","Fisioterapi1")).collect(Collectors.toList()));
		assertEquals(2, imedicoService.listar().size());
		assertEquals(2, medicoController.listar().size());
	}
	
	

}
