package com.mitocode;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.mitocode.controller.EspecialidadController;
import com.mitocode.dao.IEspecialidadDAO;
import com.mitocode.model.Especialidad;
import com.mitocode.service.IEspecialidadService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EspecialidadTest {
	
	@Autowired
	private IEspecialidadService iespecialidadService;
	
	@Autowired
	private EspecialidadController especialidadController;
	
	@MockBean
	private IEspecialidadDAO iespecialidadDao;
	
	@Test
	public void camposSetGet() {
		Especialidad especialidad = new Especialidad();
		String nombre = "Medicina General";		
		especialidad.setNombre(nombre);		
		assertEquals(especialidad.getNombre(), nombre);		
	}	
	
	@Test
	public void crearEspecialidad() {
		Especialidad especialidad = new Especialidad("Medicina General");
						
		when(iespecialidadDao.save(especialidad)).thenReturn(especialidad);
		
		assertEquals(especialidad, iespecialidadService.registrar(especialidad));
		assertEquals(especialidad, especialidadController.registrar(especialidad));
	}
	
	@Test
	public void borrarEspecialidad() {
		Especialidad especialidad = new Especialidad("Pediatria");
		iespecialidadService.eliminar(especialidad.getIdEspecialidad());
		verify(iespecialidadDao,times(1)).delete(especialidad.getIdEspecialidad());				
		
		especialidad.setNombre("Pediatria1");
		especialidadController.eliminar(especialidad.getIdEspecialidad());
	}
	
	@Test
	public void buscaEspecialidad() {
		Especialidad especialidad = new Especialidad("Medicina General");
		Integer id = 2;
		especialidad.setIdEspecialidad(id);
		Especialidad especialidad1 = iespecialidadDao.findOne(id);
		assertNull(especialidad1);
		
		Especialidad especialidad2 = iespecialidadService.listarId(id);
		assertNull(especialidad2);
		
	}
	
	@Test
	public void getEspecialidad() {		
		when(iespecialidadDao.findAll()).thenReturn(Stream.of(new Especialidad("Pediatria"),new Especialidad("Psicología")).collect(Collectors.toList()));
		assertEquals(2, iespecialidadService.listar().size());
		assertEquals(2, especialidadController.listar().size());
	}
}
